#	 PGM Library

This repository contains `JSON` formatted versions of the texts of a collection of papyri, principally Greek and secondarily Coptic, collectively referred to as the *Paypyri Graecae magicae* (PGM) --- the Greek magical papyri.

## Library Collection

The library is currently a work in progress, and contains the following papyri from the PGM collection:

|PGM Reference|Location                     |Number                       |File         |
|-------------|-----------------------------|-----------------------------|-------------|
|I            |Berlin, Staatliche Museen    |P.Berol. inv. 5025           |pgm_001.json |
|II           |Berlin, Staatliche Museen    |P.Berol. inv. 5026           |pgm_002.json |
|III          |Paris, Musée du Louvre       |no. 2396 (P.Mimaut frgs. 1-4)|pgm_003.json |
|IV           |Paris, Bibliothèque Nationale|P.Bibl.Nat.Suppl. gr. no. 547|pgm_004.json |
|V            |London, British Museum       |P.Lond. 46                   |pgm_005.json |
|Va           |Uppsala, Victoriamuseet      |P.Holm., p. 42               |pgm_005a.json|
|VI           |London, British Museum       |P.Lond. 47                   |pgm_006.json |

The remaining papyri are currently being assembled as `JSON` formatted versions, for inclusion within this library --- with the eventual goal being a complete representation of the PGM.

## JSON Structure

The high-level structure of each `JSON` object contains the following two elements:

```json
{
	"Metadata": {...},
	"Spells": [...]
}
```

The `Metadata` element within each object will contain a `Papyrus` and `Editions` elements. 

```json
{
	"Metadata": {
		"Papyrus": {},
		"Editions": []
	},
	"Spells": [...]
}
```

The `Papyrus` element is an object which contains (1) the papyrus' integer-formatted reference number, (2) the location of the papyrus, and (3) its reference number within the institutional collection:

```
{
	"Metadata": {
        "Papyrus": {
            "Reference": {
                "Order": 1.0,
                "Arabic Number": "1",
                "Roman Numeral": "I"
            },
            "Location": "Berlin, Staatliche Museen",
            "Number": "P.Berol. inv. 5025"
        },
        "Editions": [...]
    }
}
```

The `Editions` element is a list of critical and scholarly editions, broken out by bibliographic details,	 from which the text of the papyrus was derived:

```
{
	"Metadata": {...},
        "Editions": [
            {
                "Full Title": "Papyri Graecae magicae: Die griechischen Zauberpapyri",
                "Abbreviated Title": "PGM",
                "Volumes": [
                    "1", 
                    "2"
                ],
                "Edition": "2nd",
                "Editors": [
                    "Karl Preisendanz",
                    "Albert Henrichs"
                ],
                "Publisher": "Teubner",
                "Publication Location": "Stuttgart",
                "Publication Year": [
                    "1973", 
                    "1974"
                ]
            }, {
                "Title": "The Greek Magical Papyri in Translation: Including the Demotic Spells",
                "Abbreviated Title": "GMPT",
                "Edition": "2nd",
                "Editors": ["Hans Dieter Betz"],
                "Publisher": "University of Chicago Press",
                "Publication Location": "Chicago",
                "Publication Year": "1992"
            }
        ]
    }
}
```

The `Spells` element is a list which contains an object representation of the metadata and text of each spell within the papyrus, including (1) the papyrus' number, (2) the first line on which the spell begins, (3) the last line on which the spell ends, (4) the formal reference for the spell, (5) the standard English translation of the spell's title, and (6) the languages in which the spell's text is written. Each line of text is structured as an key-value store, where the key represents the line number within the papyrus, and the value is the Unicode representation of the text:

```json
{
    "Metadata": {...},
    "Spells": [
		{...}, {
            "Papyrus": I,
            "First Line": 247,
            "Last Line": 262,
            "PGM Reference": "PGM I.247-262",
            "GMPT Title": "Spell for invisibility",
            "Language": [
                "Greek", 
                "Coptic"
            ],
            "Text": {
                "247": "<Ἀμαύρωσι<ς> δοκίμη.> μέγα ἔργον· ",
                "248": "λαβὼν πιθήκου ὀφθαλμὸν ἢ νέκυος βιοθανάτου καὶ βοτάνην ἀγλαοφωτίδος (τὸ ῥόδον λέγει) ταῦτα τρί-",
                "249": "ψας σὺν ἐλαίῳ σουσίνῳ, τρί-",
                "250": "βων δὲ αὐτὰ ἐκ τῶν δεξιῶν εἰς τὰ εὐώνυμα λέγε τὸν λό-",
                "251": "γον, ὡς ὑπόκειται· ⲁⲛⲟⲕ ⲁⲛⲟⲩⲡ ⲁⲛⲟⲕ ⲟⲩⲥⲓⲣⲫⲣⲏ ⲁⲛⲟⲕ ⲱ-",
                "252": "ⲥⲱⲧ ⲥⲱⲣⲱⲛ ⲟⲩⲓⲉⲣ ⲁⲛⲟⲕ ⲡⲉ ⲟⲩⲥⲓⲣⲉ ⲡⲉⲛⲧⲁ ⲥⲏⲧ ⲧⲁⲕⲟ",
                "253": "ἀνάστηθι, δαίμων καταχθόνιε ιω Ἐρβηθ ιω Φορβηθ ιω ",
                "254": "Πακερβηθ ιω Ἀπομψ, ὃ ἐὰν ἐπιτάξω ὑμῖν ἐγὼ ὁ δεῖνα, ὅπως ",
                "255": "ἐπήκοοί μοι γένησθε.’ ἐὰν δὲ θελήσῃς ἄφαντος γενέ-",
                "256": "σθαι, χρῖσόν σου τὸ μέτωπον μόνον ἐκ τοῦ συνθέματος, ",
                "257": "καὶ ἄφαντος ἔσῃ, ἐφ' ὅσον χρόνον θέλεις. ἐὰν δὲ θελή-",
                "258": "σῃς ἐμφαίνεσθαι, ἀπὸ δύσεως ἐρχόμενος εἰς ἀνατολὴν ",
                "259": "λέγε τὸ ὄνομα τοῦτο, καὶ ἔσει δηλωτικὸς καὶ ἔποπτος πᾶσιν ",
                "260": "ἀνθρώποις. ἔστιν δὲ τὸ ὄνομα· ‘Μαρμαριαωθ μαρμα-",
                "261": "ριφεγγη, ποιήσατέ με, τὸν δεῖνα, ἔποπτον πᾶσιν ἀνθρώποις ",
                "262": "ἐν τῇ σήμερον ἡμέρᾳ, ἤδη, ἤδη, ταχύ, ταχύ.’ ἔχε<ι> λίαν καλῶς. "
            }
        }, {...}
	]
}
```

